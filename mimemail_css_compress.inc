<?php

/**
 * CSS Compressor.
 *
 * @todo Remove classes from doc that are not mentioned in non-inlinable rules.
 * @todo Rename functions to follow Drupal standards.
 * @todo Use more CSS Parser features, like sorting selectors by precedence.
 */
class MimeMailCssCompress {

  protected $html = '';

  /**
   * @var DOMXPath|null
   */
  protected $xpath = null;

  /**
   * @var DOMDocument|null
   */
  protected $doc = null;

  public function __construct($html) {
    $this->html = $html;
  }

  protected function dependenciesSatisfied() {
    if (!class_exists('DOMDocument', FALSE)) {
      return FALSE;
    }

    $parser_path = libraries_get_path('PHP-CSS-Parser') . '/lib/Sabberworm/CSS/Parser.php';
    if (!file_exists($parser_path)) {
      return FALSE;
    }

    include_once $parser_path;
    if (!class_exists('\Sabberworm\CSS\Parser')) {
      return FALSE;
    }

    return TRUE;
  }

  public function parseCSS() {
    $inlinable_css = '';

    // Disable multi-byte support for speed.
    $parser_settings = \Sabberworm\CSS\Settings::create();
    $parser_settings->withMultibyteSupport(false);

    $nodes = $this->doc->getElementsByTagName('style');
    foreach ($nodes as $node) {
      /**
       * @var DOMNode $node
       */

      $css = trim($node->nodeValue);
      $css = str_replace(array('<!--', '-->'), '', $css);

      try {
        $parser = new \Sabberworm\CSS\Parser($css, $parser_settings);
        $css_content = $parser->parse()->getContents();
      }
      catch (Exception $e) {
        // Broken CSS. Log exception and leave style tag as-is.
        watchdog_exception('mimemail_css_compress', $e);
        continue;
      }

      // Re-construct CSS that should be left in style tag and append the rest
      // to the global list of rules that are going to be inline.
      $tag_css = '';
      foreach ($css_content as $entry) {
        if ($entry instanceof \Sabberworm\CSS\RuleSet\DeclarationBlock) {
          //  (selector + rules).
          $inlinable_css .= $entry;
        }
        else {
          $tag_css .= $entry;
        }
      }

      if ($tag_css !== '') {
        $node->nodeValue = "<!--\n" . $tag_css . '-->';
      }
      else {
        $node->parentNode->removeChild($node);
      }
    }

    return $inlinable_css;
  }

  public function compress() {
    if (!$this->dependenciesSatisfied()) {
      // Some required classes are missing. Return untouched document.
      return $this->html;
    }

    // TODO Take care of wbr.

    $err = error_reporting(0);
    $doc = $this->doc = new DOMDocument();

    // Try to set character encoding.
    if (function_exists('mb_convert_encoding')) {
      $this->html = mb_convert_encoding($this->html, 'HTML-ENTITIES', "UTF-8");
      $doc->encoding = "UTF-8";
    }

    $doc->strictErrorChecking = FALSE;
    $doc->formatOutput = TRUE;
    $doc->loadHTML($this->html);
    $doc->normalizeDocument();

    $xpath = $this->xpath = new DOMXPath($doc);

    // Parse CSS from the document.
    $css = $this->parseCSS();

    // TODO Use already parsed data instead of parsing here again.

    // Process the CSS file for selectors and definitions.
    preg_match_all('/^\s*([^{]+){([^}]+)}/mis', $css, $matches);

    $all_selectors = array();
    foreach ($matches[1] as $key => $selector_string) {
      // If there is a blank definition, skip.
      if (!strlen(trim($matches[2][$key]))) continue;
      // Else split by commas and duplicate attributes so we can sort by selector precedence.
      $selectors = explode(',', $selector_string);
      foreach ($selectors as $selector) {
        // Don't process pseudo-classes.
        if (strpos($selector, ':') !== FALSE) continue;
        $all_selectors[] = array(
          'selector' => $selector,
          'attributes' => $matches[2][$key],
          'index' => $key,
          // Keep track of where it appears in the file, since order is important.
        );
      }
    }

    // Now sort the selectors by precedence.
    usort($all_selectors, array('self', 'sort_selector_precedence'));

    // Before we begin processing the CSS file, parse the document for inline
    // styles and append the normalized properties (i.e., 'display: none'
    // instead of 'DISPLAY: none') as selectors with full paths (highest
    // precedence), so they override any file-based selectors.
    $nodes = @$xpath->query('//*[@style]');
    if ($nodes->length > 0) {
      foreach ($nodes as $node) {
        $style = preg_replace_callback('/[A-z\-]+(?=\:)/S', create_function('$matches', 'return strtolower($matches[0]);'), $node->getAttribute('style'));
        $all_selectors[] = array(
          'selector' => $this->calculateXPath($node),
          'attributes' => $style,
        );
      }
    }

    foreach ($all_selectors as $value) {
      // Query the body for the xpath selector.
      $nodes = $xpath->query($this->css_to_xpath(trim($value['selector'])));

      foreach ($nodes as $node) {
        // If it has a style attribute, get it, process it, and append (overwrite) new stuff.
        if ($node->hasAttribute('style')) {
          // Break it up into an associative array.
          $old_style = $this->css_style_to_array($node->getAttribute('style'));
          $new_style = $this->css_style_to_array($value['attributes']);
          // New styles overwrite the old styles (not technically accurate, but close enough).
          $compressed = array_merge($old_style, $new_style);
          $style = '';
          foreach ($compressed as $k => $v) {
            $style .= (drupal_strtolower($k) . ':' . $v . ';');
          }
        }
        else {
          // Otherwise create a new style.
          $style = trim($value['attributes']);
        }
        $node->setAttribute('style', $style);
      }
    }

    error_reporting($err);

    return $doc->saveHTML();
  }

  protected static function sort_selector_precedence($a, $b) {
    $precedenceA = self::get_selector_precedence($a['selector']);
    $precedenceB = self::get_selector_precedence($b['selector']);

    // We want these sorted ascendingly so selectors with lesser precedence get processed first and selectors with greater precedence get sorted last.
    return ($precedenceA == $precedenceB) ? ($a['index'] < $b['index'] ? -1 : 1) : ($precedenceA < $precedenceB ? -1 : 1);
  }

  protected static function get_selector_precedence($selector) {
    $precedence = 0;
    $value = 100;
    // Ids: worth 100, classes: worth 10, elements: worth 1.
    $search = array('\#', '\.', '');

    foreach ($search as $s) {
      if (trim($selector == '')) break;
      $num = 0;
      $selector = preg_replace('/' . $s . '\w+/', '', $selector, -1, $num);
      $precedence += ($value * $num);
      $value /= 10;
    }

    return $precedence;
  }

  /**
   * Right now we only support CSS 1 selectors, but include CSS2/3 selectors are fully possible.
   *
   * @see http://plasmasturm.org/log/444
   */
  protected function css_to_xpath($selector) {
    if (drupal_substr($selector, 0, 1) == '/') {
      // Already an XPath expression.
      return $selector;
    }
    // Returns an Xpath selector.
    $search = array(
      '/\s+>\s+/',
      // Matches any F element that is a child of an element E.
      '/(\w+)\s+\+\s+(\w+)/',
      // Matches any F element that is a child of an element E.
      '/\s+/',
      // Matches any F element that is a descendant of an E element.
      '/(\w)\[(\w+)\]/',
      // Matches element with attribute.
      '/(\w)\[(\w+)\=[\'"]?(\w+)[\'"]?\]/',
      // Matches element with EXACT attribute.
      '/(\w+)?\#([\w\-]+)/e',
      // Matches id attributes.
      '/(\w+|\*)?((\.[\w\-]+)+)/e',
      // Matches class attributes.
    );
    $replace = array(
      '/',
      '\\1/following-sibling::*[1]/self::\\2',
      '//',
      '\\1[@\\2]',
      '\\1[@\\2="\\3"]',
      "(strlen('\\1') ? '\\1' : '*').'[@id=\"\\2\"]'",
      "(strlen('\\1') ? '\\1' : '*').'[contains(concat(\" \",normalize-space(@class),\" \"),concat(\" \",\"'.implode('\",\" \"))][contains(concat(\" \",normalize-space(@class),\" \"),concat(\" \",\"',explode('.',substr('\\2',1))).'\",\" \"))]'",
    );
    return '//' . preg_replace($search, $replace, trim($selector));
  }

  protected function css_style_to_array($style) {
    $definitions = explode(';', $style);
    $css_styles = array();
    foreach ($definitions as $def) {
      if (empty($def) || strpos($def, ':') === FALSE) continue;
      list($key, $value) = explode(':', $def, 2);
      if (empty($key) || empty($value)) continue;
      $css_styles[trim($key)] = trim($value);
    }
    return $css_styles;
  }

  /**
   * Get the full path to a DOM node.
   *
   * @param DOMNode $node
   *   The node to analyze.
   *
   * @return string
   *   The full xpath to a DOM node.
   *
   * @see http://stackoverflow.com/questions/2643533/php-getting-xpath-of-a-domnode
   */
  protected function calculateXPath(DOMNode $node) {
    $xpath = '';
    $q = new DOMXPath($node->ownerDocument);

    do {
      $position = 1 + $q->query('preceding-sibling::*[name()="' . $node->nodeName . '"]', $node)->length;
      $xpath = '/' . $node->nodeName . '[' . $position . ']' . $xpath;
      $node = $node->parentNode;
    } while (!$node instanceof DOMDocument);

    return $xpath;
  }

}
